import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { Routes, Route, Navigate } from "react-router-dom";
import { useEffect, useState } from "react";
import { getItems } from "./common/api";
import { setData } from "./redux/actions";
import { dbPaths } from "./common/constants";
import { Col, Row } from 'react-bootstrap';

import Companies from "./Pages/Companies/Companies";
import Projects from "./Pages/Projects/Projects";
import CompaniesProjects from "./Pages/CompaniesProjects/CompaniesProjects";
import Home from "./Pages/Home/Home";

import Navbar from "./components/Navbar/Navbar"

function App() {
  const { companies, projects, companies_projects, currentProjectId, currentCompanyId,currentCompanyProjId, isAdmin, showAddProject } =
    useSelector((state) => state);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  useEffect(() => {
    setLoading(true);
    dbPaths.forEach((path, index, paths) => {
      getItems(path)
        .then((response) => {
          console.log(response.data);
          if (response && response.data) {
            console.log('aici');
            dispatch(setData(path, response.data[path]));
          }
        })
        .catch((error) => {
          console.log("Eroare la preluare date", error);
          setError(error);
        })
        .finally(() => {
          index === paths.length - 1 && setLoading(false);
        });
    });
  }, []);
  return (
    <div className="App">
      {loading ? (
        <h1>Loading...</h1>
      ) : (
        <>
          <Row style={{ marginBottom: "80px" }}>
            <Col>
              <Navbar />
            </Col>
          </Row>
          <Row>
            <Col>
              <Routes>
                <Route
                  exact
                  path="/"
                  element={
                    <Home />
                  }
                />
                <Route
                  exact
                  path="/companies"
                  element={
                    <Companies
                      projects={projects}
                      companies={companies}
                      companies_projects={companies_projects}
                      isAdmin={isAdmin}
                      currentCompanyId={currentCompanyId}
                      showAddProject={showAddProject}
                    />
                  }
                />
                <Route
                  exact
                  path="/projects"
                  element={
                    <Projects
                      projects={projects}
                      companies={companies}
                      companies_projects={companies_projects}
                      isAdmin={isAdmin}
                      currentProjectId={currentProjectId}
                      showAddProject={showAddProject}
                    />
                  }
                />
                <Route
                  exact
                  path="/compProjects"
                  element={
                    <CompaniesProjects
                      projects={projects}
                      companies={companies}
                      companies_projects={companies_projects}
                      isAdmin={isAdmin}
                      currentCompanyProjId={currentCompanyProjId}
                      showAddProject={showAddProject}
                    />
                  }
                />
                <Route path="*" element={<Navigate from="*" to="/" />} />
              </Routes>
            </Col>
          </Row>

        </>
      )}
      {error && <h1>Erro: {"" + error}</h1>}
    </div>

  );
}

export default App;
