import React, { useEffect, useState } from "react";
import "./Companies.css";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { Modal, Button, Form } from 'react-bootstrap';
import { FaPlus } from 'react-icons/fa';
import { addItem, editItem, deleteItem } from "../../common/api";
import CompaniesArr from "../../components/Companies/Companies";
import { setShowAdd, setCompanyId } from "../../redux/actions";
import { dbPaths } from "../../common/constants";

const Companies = ({
  projects,
  companies,
  companies_projects,
  isAdmin,
  currentCompanyId,
  showAddProject,
}) => {
  const [isDelete, setIsDelete] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [currentName, setCurrentName] = useState("");
  const [currentCui, setCurrentCui] = useState("");
  const [currentNrReg, setCurrentNrReg] = useState("");

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (currentCompanyId) {
      const currentCompany = companies.find(
        (company) => company.idcomp == currentCompanyId
      );
      console.log(" currentCompany", currentCompany)
      if (currentCompany) {
        setCurrentName(currentCompany.cname);
        setCurrentCui(currentCompany.cui);
        setCurrentNrReg(currentCompany.nr_reg);
      }
    } else {
      setCurrentName("");
      setCurrentCui("");
      setCurrentNrReg("");
    }
    return () => {
      setCurrentName("");
      setCurrentCui("");
      setCurrentNrReg("");
    };
  }, [companies, currentCompanyId]);

  const handleClose = () => {
    setShow(false);
    setIsDelete(false);
    setIsEdit(false);
    setIsAdd(false);
  };
  const handleShow = () => setShow(true);
  const handleDelete = (id) => {
    console.log('Company id to be removed ', id);
    dispatch(setCompanyId(id));
    setIsDelete(true);
    setShow(true);
  };
  const handleEdit = (id) => {
    console.log("handleEdit", id)
    dispatch(setCompanyId(id));
    dispatch(setShowAdd(false));
    setIsEdit(true);
    setIsAdd(true);
    setShow(true);
  };

  const handleAdd = () => {
    setIsAdd(true);
    setShow(true);
  };


  const handleDeleteCompany = () => {
    setLoading(true);
    setError(false);
    deleteItem(dbPaths[0], currentCompanyId)
      .then((response) => {
        response && window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        setError(error);
      })
      .finally(() => setLoading(false));
  };
  const handleSubmit = () => {
    setLoading(true);
    setError(false);
    if (showAddProject) {
      console.log("ADD COMPANY!");
      addItem(dbPaths[0], {
        cname: currentName, cui: currentCui,
        nr_reg: currentNrReg
      })
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log("ADD Company!", error);
          setError(error);
        })
        .finally(() => setLoading(false));
    } else {
      editItem(dbPaths[0], currentCompanyId, {
        cname: currentName,
        cui: currentCui,
        nr_reg: currentNrReg,
        idcomp: currentCompanyId

      })
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log(error);
          setError(error);
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <>
      <h4>Companies list</h4>
      <CompaniesArr
        companies={companies} handleDelete={handleDelete}
        handleEdit={handleEdit}
      />
      <hr />
      <div style={{ cursor: 'pointer' }} className="button"
        onClick={() => {
          dispatch(setShowAdd(true));
          dispatch(setCompanyId(null));
          handleShow();
          handleAdd();
        }}>
        <span> Add company</span>
        <FaPlus />
      </div>

      <Modal show={show} onHide={handleClose} animation={false}>
        <Form>
          <Modal.Header closeButton>
            <Modal.Title>{isEdit ? "EDIT" : isAdd ? "Add" : "DELETE"} Company</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {isAdd ? (<div >
              <Form.Group controlId="formBasicWeight">
                <Form.Label className="textTransforms" >Company name</Form.Label>
                <Form.Control
                  name="name"
                  type="text"
                  placeholder="Name"
                  onChange={(e) => {
                    setCurrentName(e.target.value);
                  }}
                  value={currentName}
                />
              </Form.Group>
              <Form.Group controlId="formBasicWeight">
                <Form.Label className="textTransforms" >Company CUI</Form.Label>
                <Form.Control
                  name="cui"
                  type="text"
                  placeholder="CUI"
                  onChange={(e) => {
                    setCurrentCui(e.target.value);
                  }}
                  value={currentCui}
                />
              </Form.Group>
              <Form.Group controlId="formBasicDescriere">
                <Form.Label className="textTransforms" >Nr reg</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  name="reg"
                  placeholder="HR:000:9999"
                  onChange={(e) => {
                    setCurrentNrReg(e.target.value);
                  }}
                  value={currentNrReg}
                />
              </Form.Group>
              {loading && <p>Loading...</p>}
              {error && <p>Something went wrong</p>}
            </div>) : null}

            {isDelete ? (<p>Do you want to remove the company?</p>) : null}

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              {isDelete ? "NO" : "Close"}
            </Button>
            <Button variant="primary" onClick={isDelete ? handleDeleteCompany : handleSubmit}>
              {isDelete ? "YES" : "Submit"}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>

    </>
  );
};

export default Companies;
