import React, { useEffect, useState } from "react";
import "./CompaniesProjects.css";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { Modal, Button, Form } from 'react-bootstrap';
import { FaPlus } from 'react-icons/fa';
import { addItem, editItem, deleteItem } from "../../common/api";
import CompaniesPprojectsArr from "../../components/CompaniesPprojects/CompaniesPprojects";
import { setShowAdd, setCompanyProjId } from "../../redux/actions";
import { dbPaths } from "../../common/constants";

const CompaniesPprojects = ({
  companies,
  projects,
  companies_projects,
  isAdmin,
  currentCompanyProjId,
  showAddProject,
}) => {
  const [isDelete, setIsDelete] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [currentType, setCurrentType] = useState("");
  const [currentDate, setCurrentDate] = useState("");
  const [currentEstimate, setCurrentEstimate] = useState("");
  const [currentDesc, setCurrentDesc] = useState("");
  const [currentCompID, setCurrentCompID] = useState("");
  const [currentProjID, setCurrentProjID] = useState("");

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (currentCompanyProjId) {
      const currentProj = projects.find(
        (project) => project.idprj == currentCompanyProjId
      );
      console.log(" currentProj", currentProj)
      if (currentProj) {
        setCurrentType(currentProj.prj_type);
        setCurrentDate(currentProj.date);
        setCurrentEstimate(currentProj.estimate);
        setCurrentDesc(currentProj.desc_prj);
      }
    } else {
      setCurrentType("");
      setCurrentDate("");
      setCurrentEstimate("");
      setCurrentDesc("");
    }
    return () => {
      setCurrentType("");
      setCurrentDate("");
      setCurrentEstimate("");
      setCurrentDesc("");
    };
  }, [projects, currentCompanyProjId]);

  const handleClose = () => {
    setShow(false);
    setIsDelete(false);
    setIsEdit(false);
    setIsAdd(false);
  };
  const handleShow = () => setShow(true);
  const handleDelete = (id) => {
    console.log('project id to be removed ', id);
    dispatch(setCompanyProjId(id));
    setIsDelete(true);
    setShow(true);
  };
  const handleEdit = (id) => {
    console.log("handleEdit", id)
    dispatch(setCompanyProjId(id));
    dispatch(setShowAdd(false));
    setIsEdit(true);
    setIsAdd(true);
    setShow(true);
  };

  const handleAdd = () => {
    setIsAdd(true);
    setShow(true);
  };


  const handleDeleteproject = () => {
    setLoading(true);
    setError(false);
    deleteItem(dbPaths[2], currentCompanyProjId)
      .then((response) => {
        response && window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        setError(error);
      })
      .finally(() => setLoading(false));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log("comp proj", currentCompID, " ", currentProjID);
    setLoading(true);
    setError(false);
    if (showAddProject) {
      console.log("ADD project!");
      addItem(dbPaths[2], {
        idcomp: currentCompID, idprj: currentProjID
      })
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log("ADD project!", error);
          setError(error);
        })
        .finally(() => setLoading(false));
    } else {
      editItem(dbPaths[2], currentCompanyProjId, {
        idcomp: currentCompID, idprj: currentProjID,
        idcompProj: currentCompanyProjId

      })
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log(error);
          setError(error);
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <>
      <h4>Projects list</h4>
      <CompaniesPprojectsArr
        companies_projects={companies_projects} handleDelete={handleDelete}
        handleEdit={handleEdit}
      />
      <hr />
      <div style={{ cursor: 'pointer' }} className="button"
        onClick={() => {
          dispatch(setShowAdd(true));
          dispatch(setCompanyProjId(null));
          handleShow();
          handleAdd();
        }}>
        <span> Add</span>
        <FaPlus />
      </div>

      <Modal show={show} onHide={handleClose} animation={false}>
        <Form>
          <Modal.Header closeButton>
            <Modal.Title>{isEdit ? "EDIT" : isAdd ? "Add" : "DELETE"} Company-Project</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {isAdd ? (<div >
              <Form.Group controlId="formBasicCompanies">
                <Form.Label className="textTransforms" > Companies</Form.Label>
                <Form.Select onChange={(e) => {
                  setCurrentCompID(e.target.value);
                }}
                  value={currentCompID}>
                  <option value={"0"} >{"Select company"}</option>
                  {companies.map((comp, index) => (
                    <option key={comp.idcomp} value={comp.idcomp} >{comp.cname}</option>))}
                </Form.Select>
              </Form.Group>
              <Form.Group controlId="formBasicCompanies">
                <Form.Label className="textTransforms" > Projects</Form.Label>
                <Form.Select onChange={(e) => {
                  setCurrentProjID(e.target.value);
                }}
                  value={currentProjID} >
                  <option value={"0"} >{"Select project"}</option>
                  {projects.map((proj, index) => (
                    <option key={proj.idprj} value={proj.idprj} >{proj.prj_type}</option>))}
                </Form.Select>
              </Form.Group>
              {loading && <p>Loading...</p>}
              {error && <p>Something went wrong</p>}
            </div>) : null}

            {isDelete ? (<p>Do you want to remove the project of company?</p>) : null}

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              {isDelete ? "NO" : "Close"}
            </Button>
            <Button variant="primary" onClick={isDelete ? handleDeleteproject : handleSubmit}>
              {isDelete ? "YES" : "Submit"}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>

    </>
  );
};

export default CompaniesPprojects;
