import React from "react";
import "./Home.css";

import SlideShow from "../../components/SlideShow/SlideShow";
import { Col, Row } from "react-bootstrap";

const Home = ({
}) => {
  return (
    <>
      <Row>
        <Col>
          <SlideShow />
        </Col>
      </Row>
    </>
  );
};

export default Home;
