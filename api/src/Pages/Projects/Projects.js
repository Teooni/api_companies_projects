import React, { useEffect, useState } from "react";
import "./Projects.css";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";
import { Modal, Button, Form } from 'react-bootstrap';
import { FaPlus } from 'react-icons/fa';
import { addItem, editItem, deleteItem } from "../../common/api";
import ProjectsArr from "../../components/Projects/Projects";
import { setShowAdd, setProjectId } from "../../redux/actions";
import { dbPaths } from "../../common/constants";

const projects = ({
  companies,
  projects,
  projects_projects,
  isAdmin,
  currentProjectId,
  showAddProject,
}) => {
  const [isDelete, setIsDelete] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const [isAdd, setIsAdd] = useState(false);
  const [show, setShow] = useState(false);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [currentType, setCurrentType] = useState("");
  const [currentDate, setCurrentDate] = useState("");
  const [currentEstimate, setCurrentEstimate] = useState("");
  const [currentDesc, setCurrentDesc] = useState("");

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    if (currentProjectId) {
      const currentProj = projects.find(
        (project) => project.idprj == currentProjectId
      );
      console.log(" currentProj", currentProj)
      if (currentProj) {
        setCurrentType(currentProj.prj_type);
        setCurrentDate(currentProj.date);
        setCurrentEstimate(currentProj.estimate);
        setCurrentDesc(currentProj.desc_prj);
      }
    } else {
      setCurrentType("");
      setCurrentDate("");
      setCurrentEstimate("");
      setCurrentDesc("");
    }
    return () => {
      setCurrentType("");
      setCurrentDate("");
      setCurrentEstimate("");
      setCurrentDesc("");
    };
  }, [projects, currentProjectId]);

  const handleClose = () => {
    setShow(false);
    setIsDelete(false);
    setIsEdit(false);
    setIsAdd(false);
  };
  const handleShow = () => setShow(true);
  const handleDelete = (id) => {
    console.log('project id to be removed ', id);
    dispatch(setProjectId(id));
    setIsDelete(true);
    setShow(true);
  };
  const handleEdit = (id) => {
    console.log("handleEdit", id)
    dispatch(setProjectId(id));
    dispatch(setShowAdd(false));
    setIsEdit(true);
    setIsAdd(true);
    setShow(true);
  };

  const handleAdd = () => {
    setIsAdd(true);
    setShow(true);
  };


  const handleDeleteproject = () => {
    setLoading(true);
    setError(false);
    deleteItem(dbPaths[1], currentProjectId)
      .then((response) => {
        response && window.location.reload();
      })
      .catch((error) => {
        console.log(error);
        setError(error);
      })
      .finally(() => setLoading(false));
  };
  const handleSubmit = () => {
    setLoading(true);
    setError(false);
    if (showAddProject) {
      console.log("ADD project!");
      addItem(dbPaths[1], {
        prj_type: currentType, date: currentDate,
        estimate: currentEstimate, desc_prj: currentDesc
      })
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log("ADD project!", error);
          setError(error);
        })
        .finally(() => setLoading(false));
    } else {
      editItem(dbPaths[1], currentProjectId, {
        prj_type: currentType, date: currentDate,
        estimate: currentEstimate, desc_prj: currentDesc,
        idprj: currentProjectId

      })
        .then((response) => {
          response && window.location.reload();
        })
        .catch((error) => {
          console.log(error);
          setError(error);
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <>
      <h4>Projects list</h4>
      <ProjectsArr
        projects={projects} handleDelete={handleDelete}
        handleEdit={handleEdit}
      />
      <hr />
      <div style={{ cursor: 'pointer' }} className="button"
        onClick={() => {
          dispatch(setShowAdd(true));
          dispatch(setProjectId(null));
          handleShow();
          handleAdd();
        }}>
        <span> Add project</span>
        <FaPlus />
      </div>

      <Modal show={show} onHide={handleClose} animation={false}>
        <Form>
          <Modal.Header closeButton>
            <Modal.Title>{isEdit ? "EDIT" : isAdd ? "Add" : "DELETE"} Project</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {isAdd ? (<div >
              <Form.Group controlId="formBasicType">
                <Form.Label className="textTransforms" > Type</Form.Label>
                <Form.Control
                  name="type"
                  type="text"
                  placeholder="Proj Type"
                  onChange={(e) => {
                    setCurrentType(e.target.value);
                  }}
                  value={currentType}
                />
              </Form.Group>
              <Form.Group controlId="formBasicdate">
                <Form.Label className="textTransforms" > Start DATE</Form.Label>
                <Form.Control
                  name="date"
                  type="date"
                  onChange={(e) => {
                    setCurrentDate(e.target.value);
                  }}
                  value={currentDate}
                />
              </Form.Group>
              <Form.Group controlId="formBasicEstimate">
                <Form.Label className="textTransforms" > Estimate time</Form.Label>
                <Form.Control
                  name="type"
                  type="text"
                  placeholder=" nr of days"
                  onChange={(e) => {
                    setCurrentEstimate(e.target.value);
                  }}
                  value={currentEstimate}
                />
              </Form.Group>
              <Form.Group controlId="formBasicDescriere">
                <Form.Label className="textTransforms" >Description</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  name="reg"
                  placeholder="Short desc..."
                  onChange={(e) => {
                    setCurrentDesc(e.target.value);
                  }}
                  value={currentDesc}
                />
              </Form.Group>
              {loading && <p>Loading...</p>}
              {error && <p>Something went wrong</p>}
            </div>) : null}

            {isDelete ? (<p>Do you want to remove the project?</p>) : null}

          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              {isDelete ? "NO" : "Close"}
            </Button>
            <Button variant="primary" onClick={isDelete ? handleDeleteproject : handleSubmit}>
              {isDelete ? "YES" : "Submit"}
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>

    </>
  );
};

export default projects;
