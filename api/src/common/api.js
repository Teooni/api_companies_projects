import axios from "axios";
import { backendBaseUrl } from "../common/constants";

export const getItems = async (path) => {
  return await axios.get(`${backendBaseUrl}/${path}`);
};

export const addItem = async (path, obj) => {
  console.log("addItem", obj);
  return await axios.post(`${backendBaseUrl}/${path}`, obj);
};

export const editItem = async (path, id, obj) => {
  return await axios.put(`${backendBaseUrl}/${path}/${id}`, obj);
};

export const deleteItem = async (path, id) => {
  return await axios.delete(`${backendBaseUrl}/${path}/${id}`);
};
export const deleteMultipleItems = async (path, idclient, idprodus) => {
  return await axios.delete(`${backendBaseUrl}/${path}/${idclient}/${idprodus}`);
};
