export const reduxActions = {
  SET_DATA: "SET_DATA",
  SET_CURRENT_PROJECT_ID: "SET_CURRENT_PROJECT_ID",
  SET_CURRENT_COMPANY_ID: "SET_CURRENT_COMPANY_ID",
  SET_CURRENT_COMPANY_PROJECT_ID: "SET_CURRENT_COMPANY_PROJECT_ID",
  SET_ADMIN: "SET_ADMIN",
  SET_SHOW_ADD: "SET_SHOW_ADD",
};

export const dbPaths = ["companies", "projects", "companies_projects"];

//http://localhost:3000/posts/1

export const backendBaseUrl = "http://localhost:3003";
