import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Table } from 'react-bootstrap';
import { dbPaths } from "../../common/constants";
import { deleteMultipleItems } from "../../common/api";
import { FaTrashAlt, FaRegEdit } from 'react-icons/fa';
import "./Companies.css";

const Companies = ({
  companies,
  handleDelete,
  handleEdit
}) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const dispatch = useDispatch();

  return (
    <>
      <div>
        <hr />

        <hr />
        <Table striped bordered hover variant="dark" style={{ width: "50%", margin: "auto" }}>
          <thead>
            <tr>
              <th>Nr.crt</th>
              <th>Name</th>
              <th>cui</th>
              <th>nr_reg</th>
              <th>Delete or Edit</th>
            </tr>
          </thead>
          <tbody>
            {companies.map((comp, index) => (
              <tr key={comp.idcomp}>
                <td>{index + 1}</td>
                <td>{comp?.cname}</td>
                <td>{comp?.cui}</td>
                <td>{comp?.nr_reg}</td>
                <td style={{ cursor: 'pointer' }}>
                  <span onClick={() => handleEdit(comp.idcomp)} style={{ marginRight: "20px" }}><FaRegEdit /></span>
                  <span onClick={() => handleDelete(comp.idcomp)} style={{ marginRight: "20px" }}> <FaTrashAlt /></span>
                </td>
              </tr>

            ))}
          </tbody>
        </Table>
        <hr />
      </div>
    </>

  );
};

export default Companies;
