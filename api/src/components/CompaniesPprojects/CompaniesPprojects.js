import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Table } from 'react-bootstrap';
import { dbPaths } from "../../common/constants";
import { deleteMultipleItems } from "../../common/api";
import { FaTrashAlt, FaRegEdit } from 'react-icons/fa';
import "./CompaniesPprojects.css";

const CompaniesPprojects = ({
  companies_projects,
  handleDelete,
  handleEdit
}) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const dispatch = useDispatch();

  return (
    <>
      <div>
        <hr />

        <hr />
        <Table striped bordered hover variant="dark" style={{ width: "50%", margin: "auto" }}>
          <thead>
            <tr>
              <th>Nr.crt</th>
              <th>Company</th>
              <th>Project</th>
              <th>Start date</th>
              <th>Estimate</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {companies_projects.map((comp, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{comp?.company?.cname}</td>
                <td>{comp?.project?.prj_type}</td>
                <td>{comp?.project?.date}</td>
                <td>{comp?.project?.estimate}</td>
                <td style={{ cursor: 'pointer' }}>
                  {/* <span onClick={() => handleEdit(comp.idcompProj)} style={{ marginRight: "20px" }}><FaRegEdit /></span> */}
                  <span onClick={() => handleDelete(comp.idcompProj)} style={{ marginRight: "20px" }}> <FaTrashAlt /></span>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <hr />
      </div>
    </>

  );
};

export default CompaniesPprojects;
