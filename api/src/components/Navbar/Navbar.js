import React from "react";
import { Container, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { FaTrashAlt } from 'react-icons/fa';
import { useNavigate } from "react-router";

const MyNavbar = ({
}) => {
    const navigate = useNavigate();
    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top">
                <Container>
                    <Navbar.Brand onClick={() => navigate("/")} style={{ cursor: 'pointer' }}>MyJob</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link onClick={() => navigate("/companies")}>Companies</Nav.Link>
                            <Nav.Link onClick={() => navigate("/projects")}>Projects</Nav.Link>
                            <Nav.Link onClick={() => navigate("/compProjects")}>Companies Proj</Nav.Link>
                        </Nav>
                        <Nav>
                            <Nav.Link onClick={() => navigate("/")}>Love</Nav.Link>
                            <Nav.Link eventKey={2} onClick={() => navigate("/")}>
                                Info
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>

    );
};

export default MyNavbar;
