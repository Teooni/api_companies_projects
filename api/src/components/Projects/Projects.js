import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Table } from 'react-bootstrap';
import { dbPaths } from "../../common/constants";
import { deleteMultipleItems } from "../../common/api";
import { FaTrashAlt, FaRegEdit } from 'react-icons/fa';
import "./Projects.css";

const Projects = ({
  projects,
  handleDelete,
  handleEdit
}) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const dispatch = useDispatch();

  return (
    <>
      <div>
        <hr />
        <hr />
        <Table striped bordered hover variant="dark" style={{ width: "50%", margin: "auto" }}>
          <thead>
            <tr>
              <th>Nr.crt</th>
              <th>prj_type</th>
              <th>date</th>
              <th>estimate</th>
              <th>desc_prj</th>
              <th>Delete or Edit</th>
            </tr>
          </thead>
          <tbody>
            {projects.map((project, index) => (
              <tr key={project.idprj}>
                <td>{index + 1}</td>
                <td>{project?.prj_type}</td>
                <td>{project?.date}</td>
                <td>{project?.estimate}</td>
                <td>{project?.desc_prj}</td>
                <td style={{ cursor: 'pointer' }}>
                  <span onClick={() => handleEdit(project.idprj)} style={{ marginRight: "20px" }}><FaRegEdit /></span>
                  <span onClick={() => handleDelete(project.idprj)} style={{ marginRight: "20px" }}> <FaTrashAlt /></span>
                </td>
              </tr>

            ))}
          </tbody>
        </Table>
        <hr />
      </div>
    </>

  );
};

export default Projects;
