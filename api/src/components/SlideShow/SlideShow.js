import React from "react";
import { Carousel, Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { FaTrashAlt } from 'react-icons/fa';
import img1 from '../../Asserts/img/1.jpg';
import img2 from '../../Asserts/img/2.jpg';
import img3 from '../../Asserts/img/3.jpeg';
import "./SlideShow.css";

const SlideShow = ({
}) => {
    return (
        <>
            <Carousel fade className="SlideShow">
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={img1}
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={img2}
                        alt="Second slide"
                    />

                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>
                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src={img3}
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>
                </Carousel.Item>
            </Carousel>
        </>

    );
};

export default SlideShow;
