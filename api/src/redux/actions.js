import { reduxActions } from "../common/constants";

export const setData = (prop, value) => ({
  type: reduxActions.SET_DATA,
  prop,
  value,
});

export const setProjectId = (projectId) => ({
  type: reduxActions.SET_CURRENT_PROJECT_ID,
  projectId,
});

export const setCompanyId = (value) => ({
  type: reduxActions.SET_CURRENT_COMPANY_ID,
  value,
});

export const setCompanyProjId = (value) => ({
  type: reduxActions.SET_CURRENT_COMPANY_PROJECT_ID,
  value,
});

export const setAdmin = (value) => ({
  type: reduxActions.SET_ADMIN,
  value,
});

export const setShowAdd = (value) => ({
  type: reduxActions.SET_SHOW_ADD,
  value,
});
