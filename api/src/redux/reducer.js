import { reduxActions } from "../common/constants";

const initialState = {
  companies: [],
  projects: [],
  companies_projects: [],
  currentProjectId: null,
  currentCompanyId: null,
  currentCompanyProjId: null,
  isAdmin: false,
  showAddProject: false,
};

export const dataReducer = (state = initialState, action) => {
  switch (action.type) {
    case reduxActions.SET_DATA:
      return {
        ...state,
        [action.prop]: action.value,
      };

    case reduxActions.SET_CURRENT_PROJECT_ID:
      return {
        ...state,
        currentProjectId: action.projectId,
      };

    case reduxActions.SET_CURRENT_COMPANY_PROJECT_ID:
      return {
        ...state,
        currentCompanyProjId: action.value,
      };

    case reduxActions.SET_CURRENT_COMPANY_ID:
      return {
        ...state,
        currentCompanyId: action.value,
      };

    case reduxActions.SET_ADMIN:
      return {
        ...state,
        isAdmin: action.value,
      };
    case reduxActions.SET_SHOW_ADD:
      return {
        ...state,
        showAddProject: action.value,
      };
    default:
      return state;
  }
};
