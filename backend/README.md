# Start server
#### Steps to use app
1. Edit db connections with yours locall data(`username`, `password`, and `database`) from config>config.json
2. run in trminal `npm install` in backend folder
3. Run app : `node server.js` or `npm run server`(on changes the server is autmatly re-launch)
> We have 4 differnt tipes to start the app. See section `How to use sequelize` from below.

## How to use [sequelize](https://sequelize.org/master/manual/model-basics.html)
- The app have 4 models for Model synchronization with DB, can be find into [server.js](./server.js).
	- `app.listen()`- Launch app to listen to specified port without synchronization with db. We will use existing models.
	- `db.sequelize.sync()` - This creates the table if it doesn't exist (and does nothing if it already exists).
	- `db.sequelize.sync({ force: true })` - This creates the table, **dropping** it first if it already existed.
	- `db.sequelize.sync({ alter: true })`-  This checks what is the current state of the table in the database (which columns it has, what are their data types, etc), and then performs the necessary changes in the table to make it match the model.

Into current project directory, from terminal if you want to use migrate:
1. **init the sequelize**: `sequelize init`
2. **migration:create** : `sequelize migration:create --name create_test_tables`
3. **create all tables**: `sequelize db:migrate`
4. **remove all tables** : `sequelize db:migrate:undo:all`