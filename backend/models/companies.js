module.exports = (sequelize, DataTypes) => {
    const Companies = sequelize.define("companies", {
        idcomp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        cname: {
            type: DataTypes.STRING(45),
            allowNull: false,
            unique: 'unique_cname'
        },
        cui: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        nr_reg: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        is_deleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    });

    // used for foreignKey
    Companies.associate = function (models) {
        Companies.hasMany(models.companies_projects, { foreignKey: 'idcomp' });
    }
    return Companies;
}