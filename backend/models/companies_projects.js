module.exports = (sequelize, DataTypes) => {
    const CompProj = sequelize.define("companies_projects", {
        idcompProj: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        idcomp: {
            type: DataTypes.INTEGER,
            allowNull: false,
            unique: 'unique_idcomp_idprj'
        },
        idprj: {
            type: DataTypes.INTEGER,
            allowNull: false,
            unique: 'unique_idcomp_idprj'
        },
        is_deleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    });

    // used for foreignKey
    CompProj.associate = function (models) {
        CompProj.belongsTo(models.companies, { foreignKey: 'idcomp', onDelete: 'CASCADE' });
        CompProj.belongsTo(models.projects, { foreignKey: 'idprj', onDelete: 'CASCADE' });
    }

    return CompProj;
}