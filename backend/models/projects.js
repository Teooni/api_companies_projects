module.exports = (sequelize, DataTypes) => {
    const Projects = sequelize.define("projects", {
        idprj: {
            type: DataTypes.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true
        },
        prj_type: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.NOW
        },
        estimate: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        desc_prj: {
            type: DataTypes.STRING(45),
            allowNull: false
        },
        is_deleted: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false
        }
    });

    // used for foreignKey
    Projects.associate = function (models) {
        Projects.hasMany(models.companies_projects, { foreignKey: 'idprj' });
    }
    return Projects;
}