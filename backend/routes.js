let companiesController = require("./src/controllers/companiesController");
let projectsController = require("./src/controllers/projectsController");
let companies_projectController = require("./src/controllers/companies_projectController");

module.exports = function (app) {
  // Routers
  //based on methods defined in Controllers

  //COMPANIES
  app.get("/companies", companiesController.select); //get all the companies
  app.get("/companies/:id", companiesController.selectOne); //get one company
  app.post("/companies", companiesController.add); //add a new company
  app.put("/companies/:id", companiesController.update); //update a company
  app.delete("/companies/:id", companiesController.delete); //delete a company

  // PROJECTS
  app.get("/projects", projectsController.select); //get all projects
  app.get("/projects/:id", projectsController.selectOne);//get one project
  app.post("/projects", projectsController.add); //add a new project
  app.put("/projects/:id", projectsController.update); //update a project
  app.delete("/projects/:id", projectsController.delete); //delete a project

  // COMPANIES_PROJECTS
  app.get("/companies_projects/", companies_projectController.selectAll); //get the list with companies_project
  app.get("/companies_projects/:id", companies_projectController.select); //get the list with companies_project
  app.post("/companies_projects", companies_projectController.add); //add a record to the companies_project list
  app.delete("/companies_projects/:id", companies_projectController.delete); //delete a record from companies_project list

};
