const db = require('../../models');

class AdministrateCompanies {
    //---------Get all the companies
    async GetCompanies() {
        return await db.companies.findAll(
            {
                where: {
                    is_deleted: 0
                }
            }
        ).catch((err) => console.log("ERROR:", err));
    };
    //------------Get a single company
    async GetCompany(id) {
        return await db.companies.findAll({
            where: {
                idcomp: id,
                is_deleted: 0
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //---------Add a new company in DB
    async AddCompany(company) {
        return await db.companies.create({
            cname: company.cname,
            cui: company.cui,
            nr_reg: company.nr_reg
        }).catch((err) => console.log("ERROR:", err));
    }
    //------------Update the company's info
    async UpdateCompany(company) {
        console.log("UPDATE COMPANY!", company);
        return await db.companies.update({
            cname: company.cname,
            cui: company.cui,
            nr_reg: company.nr_reg
        }, {
            where: {
                idcomp: company.idcomp
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //--------------Delete a company
    async RemoveCompany(id) {
        console.log("DELETE COMPANY!", id);
        return await db.companies.update({
            is_deleted: 1
        }, {
            where: {
                idcomp: id
            }
        }).catch((err) => console.log("ERROR:", err));
    }

};

module.exports = AdministrateCompanies;