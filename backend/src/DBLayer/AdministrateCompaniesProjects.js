const db = require('../../models');
class AdministrateCompaniesProjects {

    //----------Get all the projects for a company
    async GetCompProjects(idComp) {
        if (idComp != 0) {
            return await db.companies_projects.findAll({
                where: {
                    idcomp: idComp,
                },
                include: [{
                    model: db.companies, // will create a left join,
                    where: {
                        is_deleted: 0
                    }
                }]
            }).catch((err) => console.log("ERROR:", err));
        }
        return await db.companies_projects.findAll({
            include: [{
                model: db.companies // will create a left join,
            }, {
                model: db.projects
            }]
        }).catch((err) => console.log("ERROR:", err));
    };
    //-----------Get CompProject
    async GetCompProject(idcompProj) {
        return await db.companies_projects.findAll({
            where: {
                idcompProj: idcompProj,
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //-----------Add a CompProject
    async AddCompProject(CompProject) {
        return await db.companies_projects.create({
            idcomp: CompProject.idcomp,
            idprj: CompProject.idprj
        }).catch((err) => console.log("ERROR:", err));
    }
    //--------------Remove a CompProject
    async RemoveCompProject(idcompProj) {
        return await db.companies_projects.destroy({
            where: {
                idcompProj: idcompProj
            }
        }).catch((err) => console.log("ERROR:", err));
    }

};

module.exports = AdministrateCompaniesProjects;