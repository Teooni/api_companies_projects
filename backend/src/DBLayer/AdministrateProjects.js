const db = require('../../models');

class AdministrateProjects {
    //---------Get all the projects from DB
    async GetProjects() {
        return await db.projects.findAll({
            where: {
                is_deleted: 0
            }
        }).catch((err) => console.log("ERROR:", err));
    };
    //----------Get a single project
    async GetProject(id) {
        return await db.projects.findAll({
            where: {
                idprj: id,
                is_deleted: 0
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //-------------Add a new project in DB
    async AddProject(project) {
        return await db.projects.create({
            prj_type: project.prj_type,
            date: project.date,
            estimate: project.estimate,
            desc_prj: project.desc_prj
        }).catch((err) => console.log("ERROR:", err));
    }
    //---------Update a project
    async UpdateProject(project) {
        return await db.projects.update({
            prj_type: project.prj_type,
            date: project.date,
            estimate: project.estimate,
            desc_prj: project.desc_prj
        }, {
            where: {
                idprj: project.idprj
            }
        }).catch((err) => console.log("ERROR:", err));
    }
    //------------Delete a project
    async RemoveProject(id) {
        return await db.projects.update({
            is_deleted: 1
        }, {
            where: {
                idprj: id
            }
        }).catch((err) => console.log("ERROR:", err));
    }
};

module.exports = AdministrateProjects;