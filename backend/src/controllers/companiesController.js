const services = require('../services/services');
//--------------------------------- SELECT ---------------------------------------//
exports.select = async function (req, res) {
  res.json({ companies: await new services().GetCompanies() });
};
//--------------------------------- --- --------------------------------------//
exports.selectOne = async function (req, res) {
  res.json({ companies: await new services().GetCompany(req.params.id) });
};
//---------------------------------INSERT----------------------------------------//
exports.add = async function (req, res) {
  res.json({ companies: await new services().AddCompany(req.body) });
};
//---------------------------------- UPDATE ---------------------------------------//
exports.update = async function (req, res) {
  res.json({ companies: await new services().UpdateCompany(req.body) });
};
//------------------------------- DELETE -----------------------------------------//
exports.delete = async function (req, res) {
  res.json({ companies: await new services().RemoveCompany(req.params.id) });
};
//---------------------------------------------------------------------------//
