const services = require('../services/services');

exports.selectAll = async function (req, res) {
  res.json({ companies_projects: await new services().GetCompProjects() });
};

exports.select = async function (req, res) {
  res.json({ companies_projects: await new services().GetCompProjects(req.params.id) });
};

exports.add = async function (req, res) {
  res.json({ companies_projects: await new services().AddCompProject(req.body) });
};

exports.delete = async function (req, res) {
  res.json({ companies_projects: await new services().RemoveCompProject(req.params.id) });
};
//---------------------------------------------------------------------------//
