const services = require('../services/services');

exports.select = async function (req, res) {
  res.json({ projects: await new services().GetProjects() });
};

exports.selectOne = async function (req, res) {
  res.json({ projects: await new services().GetProject(req.params.id) });
};

exports.add = async function (req, res) {
  res.json({ projects: await new services().AddProject(req.body) });
};

exports.update = async function (req, res) {
  res.json({ projects: await new services().UpdateProject(req.body) });
};
// delete function is used to remove a product, then redirect again to the product list view
exports.delete = async function (req, res) {
  res.json({ projects: await new services().RemoveProject(req.params.id) });
};
