const AdministrateCompanies = require('../DBLayer/AdministrateCompanies');
const AdministrateProjects = require('../DBLayer/AdministrateProjects');
const AdministrateCompaniesProjects = require('../DBLayer/AdministrateCompaniesProjects');
const StocareFactory = require('../utils/StocareFactory');
const NodeCache = require('node-cache');
/**
 * Time tto live
 */
const ttl = 60 * 60 * 1; // cache for 1 Hour
/**
 * This is outside of class because we need to have one instance of this object
 */
const Cache = new NodeCache(ttl); // Create a new cache service instance
//---------------------------------------------------------------------------//
class Services {
    /**
     * used to get data's for Companies
     */
    Companies;

    /**
    * used to get data's for Projects
    */
    Projects;

    /**
    * used to get data's for CompaniesProjects
    */
    CompaniesProjects;

    //---------------------------------------------------------------------------//
    constructor() {
        this.Companies = new StocareFactory().GetTipStocare(new AdministrateCompanies());
        this.Projects = new StocareFactory().GetTipStocare(new AdministrateProjects());
        this.CompaniesProjects = new StocareFactory().GetTipStocare(new AdministrateCompaniesProjects());
    }
    //-----------------------------------COMPANIES SERVICES --------------------------//
    async GetCompanies() {
        let response = await this.Companies.GetCompanies();
        return response;
    };
    //---------------------------------------------------------------------------//
    async GetCompany(id) {
        return await this.Companies.GetCompany(id);
    }
    //---------------------------------------------------------------------------//
    async AddCompany(company) {
        return await this.Companies.AddCompany(company);
    }
    //---------------------------------------------------------------------------//
    async UpdateCompany(company) {
        return await this.Companies.UpdateCompany(company);
    }
    //---------------------------------------------------------------------------//
    async RemoveCompany(id) {
        return await this.Companies.RemoveCompany(id);
    }
    //------------------------------ END COMPANIES SERVICES --------------------------//
    //-------------------------------Projects SERVICES --------------------------//
    async GetProjects() {
        return await this.Projects.GetProjects();
    };
    //---------------------------------------------------------------------------//
    async GetProject(id) {
        return await this.Projects.GetProject(id);
    }
    //---------------------------------------------------------------------------//
    async AddProject(project) {
        return await this.Projects.AddProject(project);
    }
    //---------------------------------------------------------------------------//
    async UpdateProject(project) {
        return await this.Projects.UpdateProject(project);
    }
    //---------------------------------------------------------------------------//
    async RemoveProject(id) {
        return await this.Projects.RemoveProject(id);
    }
    //----------------------------END Projects SERVICES --------------------------//
    //-----------------------------------COMPANI_PROJECTS SERVICES -------------------------//
    async GetCompProjects(idComp =0 ) {
        return await this.CompaniesProjects.GetCompProjects(idComp);
    };
    //---------------------------------------------------------------------------//
    async GetCompProject(id) {
        return await this.CompaniesProjects.GetCompProject(id);
    }
    //---------------------------------------------------------------------------//
    async AddCompProject(CompProject) {
        return await this.CompaniesProjects.AddCompProject(CompProject);
    }
    //---------------------------------------------------------------------------//
    async RemoveCompProject(idcompProj) {
        return await this.CompaniesProjects.RemoveCompProject(idcompProj);
    }
    //------------------------------ END COMPANIES_PROJECTS SERVICES ------------------------//
};

module.exports = Services;