const AdministrateCompanies = require('../DBLayer/AdministrateCompanies');
const AdministrateProjects = require('../DBLayer/AdministrateProjects');
const AdministrateCompaniesProjects = require('../DBLayer/AdministrateCompaniesProjects');

class StocareFactory {
    GetTipStocare(table_type) {
        if (table_type instanceof AdministrateCompanies) {
            return new AdministrateCompanies();
        }
        else if (table_type instanceof AdministrateProjects) {
            return new AdministrateProjects();
        } else {
            return new AdministrateCompaniesProjects();
        }
    }
};

module.exports = StocareFactory;